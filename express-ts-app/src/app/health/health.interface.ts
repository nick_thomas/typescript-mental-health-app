import { Request } from "express-serve-static-core";



export type Emotion =   Array<number> & {
    Excited: number,
    Sad: number,
    Happy: number,
    Fear: number,
    Angry: number,
    Bored: number,
}

export interface HealthPost extends Request {
    content?: String,
}
export interface Emotions {
    emotion: Emotion
}