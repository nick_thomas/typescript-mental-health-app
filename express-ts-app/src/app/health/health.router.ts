import { Router } from 'express';
import { postData } from '../../utils/httpsPost';
import { BASE_ENDPOINT } from './../../constants/endpoint';
import { Emotion, Emotions, HealthPost } from './health.interface';
import { transformEmotion, getMessage } from './../services/health/health';


// Export module for registering router in express app
export const router: Router = Router();

// Define your routes here
router.post(BASE_ENDPOINT + "/health", async (req: HealthPost, res) => {
  try {
    const options = {
      hostname: 'apis.paralleldots.com',
      port: 443,
      path: '/v4/emotion',
      method: 'POST',
    }
    const { body } = req;
    const { content } = body;
    if (content === undefined) {
      const message = "Content is missing";
      throw new Error(message);
    }
    const emotions: Emotions = await postData(options, content);
    const { emotion } = emotions;
    const emotionArray: Array<Emotion> = transformEmotion(emotion);
    const message = getMessage(emotionArray);
    res.status(200).send({
      message: "POST request from sample router",
      response: message
    });
  } catch (error) {
    res.status(500).send({
      message: "Internal Server Error",
      error: error.message
    });
  }
});

/* router.post("/", (req, res) => {
  res.status(200).send({
    message: "POST request from sample router"
  });
}); */

router.put("/", (req, res) => {
  res.status(200).send({
    message: "PUT request from sample router"
  });
});

router.delete("/", (req, res) => {
  res.status(200).send({
    message: "DELETE request from sample router"
  });
});


