import { Either } from 'monet';
import { toPairs, sort, map, multiply, compose, takeLast, cond, equals, flatten } from 'ramda';
import { Emotion } from '../../health/health.interface';



const sadResponse = ['Why do you feel this way?', 'Tell me more!', 'Are you alright', 'Please elucidate your feelings', 'Go on talk to me.']
const happyResponse = ['That sounds great', 'Good to know', 'Very good', 'I could not be happier']
const angryResponse = ['You sound quite aggitated', 'Try to calm down', 'It is not good to be angry', 'Please ask yourself if thats the right way to feel about this?']
const excitedResponse = ['Wow', 'Congrats', 'Great job', 'I knew you could do it']
const fearResponse = ['Try to focus on your breathing', 'I can understand, tell me more', 'Try to breathe slowly',]


const lowestTohighest = (a: Emotion, b: Emotion) => a[1] - b[1];
const multiplyBy10 = (x: number) => multiply(10, x)
const multiplyArrayby10 = (x: Emotion): Emotion => map(multiplyBy10, x) as never;
const sortArray = (x: Array<Emotion>): Array<Emotion> => sort(lowestTohighest, x);
const getLast = (x: Array<Emotion>): Array<Emotion> => takeLast(1, x);
const getRandomMsg = (arr: Array<String>) => Math.floor(Math.random() * arr.length)



export const transformEmotion = compose(getLast, sortArray, toPairs as never, multiplyArrayby10,)
export const getMessage = (x: Array<Emotion>): String => cond([
    [([first, _]) => equals('Happy', first), () => happyResponse[getRandomMsg(happyResponse)]],
    [([first, _]) => equals('Sad', first), () => sadResponse[getRandomMsg(sadResponse)]],
    [([first, _]) => equals('Excited', first), () => excitedResponse[getRandomMsg(excitedResponse)]],
    [([first, _]) => equals('Fear', first), () => fearResponse[getRandomMsg(fearResponse)]],
    [([first, _]) => equals('Angry', first), () => angryResponse[getRandomMsg(angryResponse)]],
])(flatten(x));