import app from './server';
import config from '../config.json';
import * as dotenv from 'dotenv';
import { MikroORM } from '@mikro-orm/core';

// Start the application by listening to specific port
dotenv.config();
const port = Number(process.env.PORT || config.PORT || 8080);
app.listen(port, () => {
  console.info('Express application started on port: ' + port);
});
// Start the database 
(async () => {
  try {
    const orm = await MikroORM.init({
      dbName: process.env.POSTGRES_DB,
      type: 'postgresql', // one of `mongo` | `mysql` | `mariadb` | `postgresql` | `sqlite`
      clientUrl: `postgresql://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@]${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.POSTGRES_DB}`, // defaults to 'mongodb://localhost:27017' for mongodb driver
    });
    console.log(orm);
  } catch (e) {
    console.log(e);
  }
})()

