import https from 'https';
import FormData from 'form-data';
import { Lens, lensProp, reject, set } from 'ramda';
import { RequestOptions } from 'node:https';
import { Emotions } from '../app/health/health.interface';

export interface HTTP_POST {
    hostname: string,
    port: number,
    path: string,
    method: string,
}

export const postData = (options: HTTP_POST, inboundData: String) : Promise<Emotions> => {
    return new Promise((resolve, reject) => {
        const form = new FormData();
        form.append('api_key', process.env.api_key);
        form.append('text', inboundData);
        const keyValue: never = 'headers' as never;
        const headers = lensProp(keyValue) as never;
        const appendOptions: RequestOptions = set(headers, form.getHeaders(), options) as RequestOptions;
        let output;
        const req = https.request(appendOptions, (res) => {
            let data: string;
            res.on('data', chunk => {
                data = chunk
            }
            );
            res.on('end', () => {
                output = data;
                resolve(JSON.parse(output));
            })
        });
        req.on('error', (err: Error) => {
            reject(err);
        })
        form.pipe(req);
        req.end();
    })
}
